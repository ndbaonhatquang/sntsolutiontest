function f() {
  let d = new Date();
  console.log(d.getMilliseconds());
  return d.getMilliseconds() % 2 === 0;
}
function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

async function retry(func, time, options) {
  if (options.max === 1) {
    return func()
  };
  if (func() === true) return true;
  options.max = options.max - 1
  await sleep(time)
  return retry(func, time, options)
}

console.log(retry(f, 1000, { max: 4 }));